import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import  { environment } from '../../environments/environment';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Register } from '../models/register';
import { REGISTER } from '../mocks/register';

import { Login } from '../models/login';


import { responseRegister } from '../models/responseregister';
import { RESPONSEREGISTER } from '../mocks/responseregister';

import { responseValidate } from '../models/responsevalidate';
import { RESPONSEVALIDATE } from '../mocks/responsevalidate';

let header = {
	'Accept': 'application/json',
	'Authorization': ''
};

const httpOptions = {
	headers: new HttpHeaders({
		'Accept': 'application/json'
	})
};

@Injectable({
	providedIn: 'root'
})
export class DeLeyesService {

	private apiUrl: string;
	private registerPath: string;
	private validatePath: string;
	private loginPath:string;

	private error:any;

	constructor(private http: HttpClient) {
		this.apiUrl = environment.apiUrl;
		this.registerPath = this.apiUrl + 'register';
		this.validatePath = this.apiUrl + 'validate';
		this.loginPath = this.apiUrl  + 'login';
	}

	register(userRegister:Register): Observable<responseRegister>{
		console.log({"Datos Usuario": userRegister,'globales':this.apiUrl});
		//return of(RESPONSEREGISTER[0]);

		let values = userRegister;
		return this.http.post<any>(this.registerPath, values, httpOptions).pipe(
			tap((register: any) => this.log(`fetched register`)),
			catchError(this.handleError<any>('setRegister'))
			);
	}

	validate(hash:string): Observable<responseValidate[]>{
		console.log({"Datos Registro": hash});
		//return of(RESPONSEVALIDATE);

		return this.http.get<any>(this.validatePath + "/" + hash,  httpOptions).pipe(
			tap((token: any) => this.log(`fetched validate`)),
			catchError(this.handleError<any>('setValidate'))
			);
	}


	login(userLogin:Login): Observable<responseRegister>{
		let values = userLogin;
		return this.http.post<any>(this.loginPath, values, httpOptions).pipe(
			tap((login: any) => this.log(`fetched login`)),
			catchError(this.handleError<any>('Log in'))
			);
	}


	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			this.error = error;

			// TODO: better job of transforming error for user consumption
			//this.log(`${operation} failed: ${error.message}`);

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

	/** Log a HeroService message with the MessageService */
	private log(message: string) {
		//this.messageService.add('HeroService: ' + message);
	}


}
