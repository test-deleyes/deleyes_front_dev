import { TestBed, inject } from '@angular/core/testing';

import { DeLeyesService } from './de-leyes.service';

describe('DeLeyesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeLeyesService]
    });
  });

  it('should be created', inject([DeLeyesService], (service: DeLeyesService) => {
    expect(service).toBeTruthy();
  }));
});
