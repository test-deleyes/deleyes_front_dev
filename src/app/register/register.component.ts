import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Register } from '../models/register';

import { DeLeyesService } from '../services/de-leyes.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

	public UserRegister: Register;
	public showError:boolean = false;
	public passMatchError:boolean = false;
	public responseRegister:any;
	public errorRegister:any

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private deLeyesService: DeLeyesService,
		) { }

	ngOnInit() {
		this.UserRegister = {
			nombre: "",
			apellido: "",
			tipo_doc: 1,
			documento: "",
			email: "",
			telefono: "",
			password: "",
			confirm_password: ""
		};
	}

	register(from){
		this.showError = false;
		this.passMatchError = false;

		if(from.valid === true && this.UserRegister.password == this.UserRegister.confirm_password){
			this.UserRegister.password = btoa(this.UserRegister.password);
			this.UserRegister.confirm_password = this.UserRegister.password;

			this.deLeyesService.register(this.UserRegister)
			.subscribe(register => {
				if(typeof register != 'undefined' && register.status == true){
					this.responseRegister = register;
					this.router.navigate(['/home']);
				}else{
					this.errorRegister = JSON.stringify(register.error);
				}
			});
		}else{
			this.showError = !from.valid;

			if(this.UserRegister.password != this.UserRegister.confirm_password){
				this.passMatchError = true;
			}
		}
	}

}
