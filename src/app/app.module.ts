import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './app-routing/app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { UserValidateComponent } from './user-validate/user-validate.component';

@NgModule({
	declarations: [
	AppComponent,
	LoginComponent,
	DashboardComponent,
	NavbarComponent,
	FooterComponent,
	RegisterComponent,
	UserValidateComponent
	],
	imports: [
	BrowserModule,
	AppRoutingModule,
	FormsModule,
	ReactiveFormsModule,
	HttpClientModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
