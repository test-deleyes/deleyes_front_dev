export interface Register {
	nombre:string;
	apellido:string;
	tipo_doc:number;
	documento:string;
	email:string;
	telefono:string;
	password:string;
	confirm_password:string;
}