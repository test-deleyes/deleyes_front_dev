import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { DeLeyesService } from '../services/de-leyes.service';

@Component({
	selector: 'app-user-validate',
	templateUrl: './user-validate.component.html',
	styleUrls: ['./user-validate.component.css']
})
export class UserValidateComponent implements OnInit {

	public registro: string;
	public error: string;
	public errorMessage: string;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private deLeyesService: DeLeyesService
		) {
		console.log(this.route.params);
		//this.hash = this.route.params.map(p => p.hash);
	}

	ngOnInit() {
		this.registro = "Se esta validando el registro";
		this.validate();
	}

	validate() {
		this.error = '';
		this.route.params.subscribe(params => {
			console.log(params);
			this.deLeyesService.validate(params.hash)
			.subscribe(validate => {
				let valu : any = validate;
				if(valu.status == true){
					this.registro = "El usuario: " + valu.user.email + " fue registrado correctamente";

				}else{
					this.registro = "No se pudo procesar la petición";
					this.error = JSON.stringify(valu.error);
				}
				console.log(valu);
				/*this.registro = validate[number].message;
				this.error = !validate[number].status;
				this.errorMessage = validate[number].error;*/
			});
		});


	}

}
