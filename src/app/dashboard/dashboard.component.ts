import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { DeLeyesService } from '../services/de-leyes.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	private rol: number;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private deLeyesService: DeLeyesService
		) {
		this.rol = +localStorage.getItem('user_rol');
	}

	ngOnInit() {
	}

}
