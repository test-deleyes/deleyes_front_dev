import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { UserValidateComponent } from '../user-validate/user-validate.component';

const routes: Routes = [
{ path: 'home', component: DashboardComponent},
{ path: 'login', component: LoginComponent},
{ path: 'register', component: RegisterComponent},
{ path: 'validate/:hash', component: UserValidateComponent},
{ path: '', redirectTo:'/home', pathMatch: 'full'},
{ path: '**', component: DashboardComponent}
];

@NgModule({
	imports: [
	RouterModule.forRoot(routes)
	],
	exports: [
	RouterModule
	]
})
export class AppRoutingModule { }
