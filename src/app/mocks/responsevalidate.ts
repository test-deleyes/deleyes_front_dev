import { responseValidate } from '../models/responsevalidate';

export const RESPONSEVALIDATE: responseValidate[] = [
{
	status: true,
	message: "Registro correcto",
	error: "OK"
},
{
	status: false,
	message: "Registro no fue posible",
	error: "El hash no es valido"
}
];