import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Login } from '../models/login';

import { DeLeyesService } from '../services/de-leyes.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	public userData: Login;
	public errorLogin: any;

	private responseLogin: any;
	private localStorageService: any;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private deLeyesService: DeLeyesService
		) {
		this.localStorageService = localStorage;
	}

	ngOnInit() {
		this.userData = {
			email: "",
			password: ""
		};
	}

	login(form){
		if(form.valid == true){
			this.deLeyesService.login(this.userData)
			.subscribe(login => {
				if(typeof login != 'undefined' && login.status == true){
					this.responseLogin = login;
					console.log(login);

					this.localStorageService.setItem('token_data', this.responseLogin.token);
					this.localStorageService.setItem('user_id', this.responseLogin.user.id);
					this.localStorageService.setItem('user', JSON.stringify(this.responseLogin.user));
					this.localStorageService.setItem('user_rol', this.responseLogin.user.rol);

					this.router.navigate(['/home']);
				}else{
					this.errorLogin = "Usuario o contraseña incorrectos";
				}
			});
		}else{
			this.errorLogin = "Ingrese los datos para loguearse";
		}

	}

}
